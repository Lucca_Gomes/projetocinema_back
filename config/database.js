const { Sequelize } = require("sequelize");

const sequelize = new Sequelize({
    dialect: "sqlite",
    storage: "./data/database.sqlite",
});

async function testConnectionDatabase() {
    try {
        await sequelize.authenticate()
    } catch (error) {
        console.log("Couldn't connect to Database")
    }
}

testConnectionDatabase();

module.exports = sequelize;
