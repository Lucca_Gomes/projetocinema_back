const Assento = require("../models/assento");

//Checando se o assento já foi selecionado
async function checkseatchosen(request, response, next) {
    try {
        const assento = request.assento;
        if(assento.ocupado === true) {
            return response.status(400).json("Assento já selecionado!");
        }
        request.assento = assento;
        next();
    } catch (error) {
        return response.status(500).json({ error: "Erro ao verificar se assento está ocupado!"});
    }

}


module.exports = checkseatchosen;


