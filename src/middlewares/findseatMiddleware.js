const Assento = require("../models/assento");

// Buscando assento por cpf
async function findSeat(request, response, next) {
    try {
        const { id } = request.params;
        const assento = await Assento.findByPk(id);
        if(!assento) {
            return response.status(400).json({ error: "Não foi possível achar assento!"});
        }
        request.assento = assento;
        next();
    } catch (error) {
        return response.status(500).json({ error: "Erro ao tentar buscar assento"});
    }
}


module.exports = findSeat;

