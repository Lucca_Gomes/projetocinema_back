const Usuario = require("../models/usuario");

async function findUser(request, response, next) {
    try {
        const { id } = request.params;
        const user = await Usuario.findByPk(id);
        if(!user) {
            return response.status(400).json({ error: "User not Found!"});
        }
        request.user = user;
        next();
    } catch (error) {
        return response.status(500).json({ error: "Erro ao checar se usuário existe!" });
    }
}

module.exports = findUser;