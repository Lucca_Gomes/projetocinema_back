const Filme = require('../models/filme');

async function checkFilmExists(request, response, next) {
    try {
        const { titulo } = request.body;
        const filme = await Filme.findOne({ where: {titulo,}});
        if(filme){
            return response.status(400).json({error: "Film Already Exists!"});
        }
        return next();
    } catch (error) {
        return response.status(500).json({ error: "Erro ao checar se filme existe!"});
    }
}

module.exports = checkFilmExists;