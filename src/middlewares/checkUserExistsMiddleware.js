const Usuario = require("../models/usuario");

async function checkUserExists(request, response, next) {
    try {
        const { cpf } = request.body;
        const usuario = await Usuario.findOne({ where: {cpf,}});
        if(usuario) {
            return response.status(400).json({ error: "User already exists!" });
        }
        return next();
    } catch (error) {
        return response.status(500).json({ error: "Erro ao checar se usuario existe!" });
    }
}

module.exports = checkUserExists;