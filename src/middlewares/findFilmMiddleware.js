const Filme = require("../models/filme");

async function findFilm(request, response, next) {
    try {
        const { id } = request.params;
        const filme = await Filme.findByPk(id);
        if(!filme) {
            return response.status(404).json({error: "Film not Found!"});
        }
        request.filme = filme;
        return next();
    } catch (error) {
        return response.status(500).json({ error: "Erro ao buscar filme!"});
    }
}

module.exports = findFilm;