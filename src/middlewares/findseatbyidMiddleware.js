const seatController = require('../controllers/seatController');

// Buscando assento por id
async function findSeatbyid(request, response, next) {
    try {
        const { id } = request.params;

        const seat = await assento.findByPK(id);
        // const seat = seatController.seatArray.find((seat) => seat.id === id);
    
        if(!seat) {
            return response.status(404).json({error: "seat not Found!"});
        }
    
        request.seat = seat;
        return next();
    } catch (error) {
        return response.status(500).json({error: "Internal server error"});
    }

}

module.exports = findSeatbyid;