const { Sequelize } = require("sequelize");

const sequelize = new Sequelize({
    dialect: "sqlite",
    storage: "./data/database.sqlite"
});

async function testConectionDatabase() {
    try {
        await sequelize.authenticate();
    } catch (error) {
        console.log("not possible to connect with database.", error);
    }
}

testConectionDatabase();

module.exports = sequelize;