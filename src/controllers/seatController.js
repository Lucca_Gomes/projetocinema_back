const Assento = require("../models/assento");

//Reservando assento
async function addSeat(request, response) {
    try {
        
        const { cpfOcupante, nomeOcupante } = request.body;
        const assento = request.assento;
        await assento.update ({
            cpfOcupante,
            nomeOcupante,
            ocupado : true,
        })
        return response.status(201).json("Assento reservado!");
    } catch (error) {
        return response.status(400).json({ error: "Erro ao reservar assento"});
    }
}
//     try {
//         const { numero, fileira, cpfOcupante, nomeOcupante, idSessao } = request.body;
//         const assento = Assento.create({
//             numero,
//             fileira,
//             preco: 22.5,
//             ocupado: true,
//             cpfOcupante,
//             nomeOcupante,
//             idSessao,
//         });
//         return response.status(201).json("Assento reservado!");
//     } catch (error) {
//         return response.status(400).json({ error: "Erro ao reservar assento"});
//     }
// }


//Listar assentos de uma sessão
async function listSeatsSection(request, response) {
    try {
        const { idSessao } = request.params;
        const assentosSessao = await Assento.findAll({ where: {idSessao,}});
        return response.status(200).json(assentosSessao);
    } catch (error) {
        return response.status(400).json({ error: "Erro ao Listar os assentos"});
    }
}

//Excluir seleção de assento
async function deleteSeat(request, response) {
    try {
        const assento = request.assento;
        await assento.destroy();
        return response.status(200).send();
    } catch (error) {
        return response.status(400).json({ error: "Erro ao excluir o assento!"});
    }
}

async function deleteAllAssento(request, response) {
    try {
        await Assento.destroy({where: {}});
        return response.status(200).send();
    } catch (error) {
        return response.status(404).json({error: "Error finding session"});
    }
}

module.exports = {
    addSeat,
    listSeatsSection,
    deleteSeat,
    deleteAllAssento,
}