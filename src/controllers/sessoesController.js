const Sessoes = require('../models/sessoes');
const Assento = require('../models/assento');
const { v4: uuidv4 } = require("uuid");

// Adicionar sessão
async function addSessao(request, response) {
    try {
        const id = uuidv4();
        const { filmeId, horario, cidade, bairro, tipo } = request.body;
        const newSessao = await Sessoes.create({id: id,filmeId, horario, cidade, bairro, tipo});
        const fileira = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];
        const coluna = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18];
        for (var i = 0; i < fileira.length; i++) {
            for (var j = 0; j < coluna.length; j++) {
                const assento = await Assento.create({
                    numero: coluna[j], 
                    fileira: fileira[i], 
                    preco: 22.5,
                    idSessao: id,
                });
            }
        }
        return response.status(201).json(newSessao);
    } catch(error){
        console.log(error)
        return response.status(400).json({error: "Error creating new session"});
    } 
}

// Listar sessões 
async function listSessao(request, response) {
    return response.status(200).json(await Sessoes.findAll());
}

// Buscar sessão pelo Id
async function getSessaoById(request, response) {
    const {id} = request.params;
    const idSessao = await Sessoes.findByPk(id);
    return response.status(200).json(idSessao);
}

// Ver Sessões por Filme
async function getSessaobyFilm(request, response){
    const {SessaoFilme} = request.params;
    const filmeSessao = await Sessoes.findAll({
        where: {
            FilmeFilmeId: SessaoFilme, 
        }
    });
console.log(filmeSessao);
return response.status(200).json(filmeSessao);
}

// Ver Sessões por Tipo
async function getSessaobyType(request, response){
    const {SessaoTipo} = request.params;
    const tipoSessao = await Sessoes.findAll({
        where: {
            tipo: SessaoTipo, 
        }
    });
console.log(tipoSessao);
return response.status(200).json(tipoSessao);
}

// Deletar sessão 
async function deleteSessao(request, response) {
    const {id} = request.params;
    const idSessao = await Sessoes.findByPk(id);
    try{
        idSessao.destroy()
        return response.status(200).json();
    } catch(error){
        return response.status(404).json({error: "Error finding session"});
    } 
}

async function deleteAllSessao(request, response) {
    try {
        await Sessoes.destroy({where: {}});
        return response.status(200).send();
    } catch (error) {
        return response.status(404).json({error: "Error finding session"});
    }
}


module.exports = {
    listSessao,
    addSessao,
    getSessaoById,
    getSessaobyFilm,
    getSessaobyType,
    deleteSessao,
    deleteAllSessao,
};