const Filme = require('../models/filme');

//Adicionar Filme
async function addFilm(request, response) {
    try {
        const { titulo, imgURL, sinopse, genero, classificacao, diretor } = request.body;
        const filme = await Filme.create({
            titulo,
            imgURL,
            sinopse,
            genero,
            classificacao,
            diretor,
        });
        return response.status(201).json("Filme adicionado!");
    } catch (error) {
        return response.status(400).json({ error: "Não foi possível adicionar o filme!"});
    }
}

//Listar Filme
async function listFilm(request, response) {
    try {
        const { titulo, genero, classificacao } = request.query;
        const whereClause = {};
        if (titulo) {
            whereClause.titulo = titulo;
        };
        if (genero) {
            whereClause.genero = genero;
        }
        if (classificacao) {
            whereClause. classificacao = classificacao;
        }
        const filmes = await Filme.findAll({
            where: whereClause,
        });
        return response.status(200).json(filmes);
    } catch (error) {
        return response.status(400).json({ error: "Não foi possível listar os filmes!"});
    }
}


//Buscar Filme por id
function getFilmById(request, response) {
    try {
        const filme = request.filme;
        return response.status(200).json(filme);
    } catch (error) {
        return response.status(400).json({ error: "Erro ao achar o filme"});
    }
}


//Excluir Filme
async function deleteFilm(request, response) {
    try {
        const filme = request.filme;
        await filme.destroy();
        return response.status(200).send();
    } catch (error) {
        return response.status(400).json({ error: "Film not found!"});
    }
}

module.exports = {
    addFilm,
    listFilm,
    getFilmById,
    deleteFilm,
}