const Usuario = require("../models/usuario");

async function addUser(request, response) {
    try {
        const { nome, sobrenome, cpf, data_nascimento, nome_usuario, email, senha } = request.body;
        const usuario = await Usuario.create({
            nome,
            sobrenome,
            cpf,
            data_nascimento,
            nome_usuario,
            email,
            senha,
        });
        return response.status(201).json("Usuário Adicionado!");
    } catch (error) {
        return response.status(400).json({ error: "Não foi possível adicionar o usuário!"});
    }
}

async function listUsers(request, response) {
    try {
        const usuarios = await Usuario.findAll();
        return response.status(200).json(usuarios);
    } catch (error) {
        return response.status(400).json({ error: "Não foi possível listar os usuários!"});
    }
}


async function getUserById(request, response) {
    try {
        const user = request.user;
        return response.status(200).json(user);
    } catch (error) {
        return response.status(400).json({ error: "Não foi possível achar o usuário!"});
    }
}

async function deleteUser(request, response) {
    try {
        const user = request.user;
        await user.destroy();
        return response.status(200).send();
    } catch (error) {
        return response.status(400).json({ error: "Não foi possível excluir o usuário!"});
    }
}

module.exports = {
    addUser,
    listUsers,
    getUserById,
    deleteUser,
}