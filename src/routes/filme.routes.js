const express = require('express');
const filmeRoutes = express.Router();


//Inclusão dos Middlewares
const checkFilmExists = require('../middlewares/checkFilmExistsMiddleware');
const findFilm = require('../middlewares/findFilmMiddleware');


//Incluir Controllers
const filmeController = require('../controllers/filmeController');


//Adicionando Filme
filmeRoutes.post('/', checkFilmExists, (request, response) => filmeController.addFilm(request, response));

//Listando Filmes
filmeRoutes.get('/', (request, response) => filmeController.listFilm(request, response));

//Buscar Filme por id
filmeRoutes.get('/:id', findFilm, (request, response) => filmeController.getFilmById(request, response));

//Excluir Filme
filmeRoutes.delete('/:id', findFilm, (request, response) => filmeController.deleteFilm(request, response));

module.exports = filmeRoutes;