const express = require('express');
const seatRoutes = express.Router();


//Inclusão dos Middlewares
const checkseatchosen = require('../middlewares/checkseatchossen');
const findSeat = require('../middlewares/findseatMiddleware');
const findSeatbyid = require('../middlewares/findseatbyidMiddleware');

//Incluir Controllers
const seatController = require('../controllers/seatController');


//Selecionar assento
seatRoutes.patch('/:id', findSeat, checkseatchosen, (request, response) => seatController.addSeat(request, response));

//Listando lugares
seatRoutes.get('/:idSessao', (request, response) => seatController.listSeatsSection(request, response));

// Buscar assento por id
seatRoutes.get('/:id', findSeatbyid, (request, response) => seatController.getseatbyid(request, response));

//Excluir seleção de assento
seatRoutes.delete('/:id', findSeat, (request, response) => seatController.deleteSeat(request, response));

//Excluir Todos assentos
seatRoutes.delete('/', (request, response) => seatController.deleteAllAssento(request, response));

module.exports = seatRoutes;