const express = require('express');
const usuarioRoutes = express.Router();

//Inclusão de Middlewares
const checkUserExists = require('../middlewares/checkUserExistsMiddleware');
const findUser = require('../middlewares/findUserMiddleware');
//Inclusão do Controller
const usuarioController = require('../controllers/usuarioController');

//Criação do usuário
usuarioRoutes.post('/', checkUserExists, (request, response) => usuarioController.addUser(request, response));

//Listar Usuários
usuarioRoutes.get('/', (request, response) => usuarioController.listUsers(request, response));

//Buscar usuário por id
usuarioRoutes.get('/:id', findUser, (request, response) => usuarioController.getUserById(request, response));

//Excluir Usuário
usuarioRoutes.delete('/:id', findUser, (request, response) => usuarioController.deleteUser(request, response));

module.exports = usuarioRoutes;