const express = require('express');
const sessoesRoutes = express.Router();


//Incluir Controllers
const sessoesController = require('../controllers/sessoesController');


//Adicionando Sessão
sessoesRoutes.post('/', (request, response) => sessoesController.addSessao(request, response));

//Listando Sessões
sessoesRoutes.get('/', (request, response) => sessoesController.listSessao(request, response));

//Buscar Sessões por Id
sessoesRoutes.get('/:id', (request, response) => sessoesController.getSessaoById(request, response));

// Ver Sessão por Filme
sessoesRoutes.get('/byFilme/:SessaoFilme', (request,response) => sessoesController.getSessaobyFilm(request, response));

// Ver Sessão por Tipo
sessoesRoutes.get('/byTipo/:SessaoTipo', (request,response) => sessoesController.getSessaobyType(request, response));

//Excluir Sessão
sessoesRoutes.delete('/:id', (request, response) => sessoesController.deleteSessao(request, response));

//Excluir Sessão
sessoesRoutes.delete('/', (request, response) => sessoesController.deleteAllSessao(request, response));


module.exports = sessoesRoutes;
