const Router = require('express');
const filmeRoutes = require('./filme.routes');
const seatRoutes = require('./seat.routes');
const sessoesRoutes = require('./sessoes.routes');
const usuarioRoutes = require('./usuario.routes');
const router = Router();

const populate = require('../../scripts/populate');

router.use('/filmes', filmeRoutes);
router.use('/sessoes', sessoesRoutes);
router.post('/populate', (request, response) => populate.populateSessoes(request, response));
router.use('/usuarios', usuarioRoutes);
router.use('/assentos', seatRoutes);

module.exports = router;