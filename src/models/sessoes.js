const { DataTypes } = require("sequelize");
const Filme = require("./filme");

const sequelize = require("../../config/database");

const Sessoes = sequelize.define("Sessoes", {
    id: {
        type: DataTypes.UUIDV4,
        allowNull: false,
        primaryKey: true,
    },

    horario: {
        type: DataTypes.STRING,
        validate: {
            hora(value) {
                try{
                    if(value.length != 5) {
                        throw new Error("Not in format");
                    };
                    const left = parseInt(value.slice(0,2));
                    const middle = value[2];
                    const right = parseInt(value.slice(3,5));
                    if(left < 0 || left > 23 || middle != ":" || right < 0 || right > 59 ) {
                        throw new Error("Not in format");
                    }
                }catch(error){
                    throw new Error("Not in format");
                }
            }
        }
    },

    cidade: {
        type: DataTypes.STRING,
    },

    bairro: {
        type: DataTypes.STRING,
    },

    tipo: {
        type: DataTypes.INTEGER,
        validate: {
            max:{
                args: [2],
                msg:"Type out of range"
            },
            min:{
                args: [0],
                msg:"Type out of range"
            }
        }
    },
    filmeId: {
        type: DataTypes.UUIDV4,
        allowNull: false,
    }
});

Sessoes.belongsTo(Filme);
// Sessoes.hasMany(Assentos);

module.exports = Sessoes
