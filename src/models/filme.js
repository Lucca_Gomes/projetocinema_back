const { DataTypes } = require('sequelize');
const { v4: uuidv4 } = require('uuid');

const sequelize = require('../../config/database');

const Filme = sequelize.define('Filme', {
    id: {
        type: DataTypes.UUIDV4,
        defaultValue: () => uuidv4(),
        primaryKey: true,
    },
    titulo: {
        type: DataTypes.STRING,
        allowNull:false,
    },
    imgURL: {
        type: DataTypes.TEXT,
        allowNull: false,
    },
    sinopse: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    genero: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    classificacao: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
            min: 1,
            max: 5,
        },
    },
    diretor: {
        type: DataTypes.STRING,
        allowNull: false,
    },
});

module.exports = Filme;