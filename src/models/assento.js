const { DataTypes } = require('sequelize');
const { v4: uuidv4 } = require('uuid');

const sequelize = require('../../config/database');

const Assento = sequelize.define('Assento', {
    id: {
        type: DataTypes.UUIDV4,
        defaultValue: () => uuidv4(),
        primaryKey: true,
    },
    numero: {
        type: DataTypes.INTEGER,
        validate:{
            min: 1,
            max: 18,
        },
        allowNull: false,
    },
    fileira: {
        type: DataTypes.STRING,
        validate: {
            isIn: [['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']],
        },
        allowNull: false,
    },
    preco: {
        type: DataTypes.FLOAT,
        allowNull: false,
    },
    ocupado: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
    },
    cpfOcupante: {
        type: DataTypes.STRING(11),
        defaultValue: null,
    },
    nomeOcupante: {
        type: DataTypes.STRING,
        defaultValue: null,
    },
    idSessao: {
        type: DataTypes.UUIDV4,
        allowNull: false,
    },
});

module.exports = Assento;