const sequelize = require("../config/database");


async function createDataBase() {
    try {
        console.log("Database created");
    } catch(error) {
        console.log(error);
    }
}

module.exports = createDataBase;