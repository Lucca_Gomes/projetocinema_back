const sequelize = require('../config/database');

async function createDatabase() {
    try {
        await sequelize.sync({ alter: true });
        console.log("Database successfully created");
    } catch (error) {
        console.log("Error creating Database", error);
    }
}

module.exports = createDatabase
