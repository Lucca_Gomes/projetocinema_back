const Sessoes = require("../src/models/sessoes");

// Criar Sessões aleatórias
async function populateSessoes(request, response) { 
    const horario = ["13:10", "14:30", "15:00", "16:20", "17:30", "18:20", "19:00", "20:10", "20:30", "21:20"];
    const cidade = ["Niterói", "Maricá"];
    const bairro = [["Centro", "Icaraí", "Itaipu"],["Eldorado", "Itapeba"]];
    const tipo = [0, 1 ,2];
    const { filmeId } = body.request; 

    for (let x = 0; x <= 2; x++) {
        const indcidade = rng(0,1); 
        const indbairro = rng(0,bairro[indcidade].length-1);
        const moreSessoes = await Sessoes.create({
            horario: horario[rng(0,9)], 
            cidade: cidade[indcidade], 
            bairro: bairro[indcidade][indbairro], 
            tipo: x,
            FilmeFilmeId: filmeId 
        });
    }

    for (let x = 1; x <= rng(0,8); x++) {
        const indcidade = rng(0,1); 
        const indbairro = rng(0,bairro[indcidade].length-1);
        const moreSessoes = await Sessoes.create({
            horario: horario[rng(0,9)], 
            cidade: cidade[indcidade], 
            bairro: bairro[indcidade][indbairro], 
            tipo: tipo[rng(0,2)], 
        });
    } 
    return response.status(200).json();
}

function rng(min, max) { 
    return Math.floor(Math.random() * (max - min + 1) + min)
}


module.exports = {populateSessoes};